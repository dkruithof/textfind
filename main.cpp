#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include <map>
#include <algorithm>
#include <vector>
#include <chrono>

using namespace std;

unsigned int addWord(std::map<std::string, unsigned int>& wordLookup, std::string word)
{
    std::transform(word.begin(), word.end(), word.begin(), ::tolower);

    auto it = wordLookup.find(word);
    unsigned int id;
    if (it == wordLookup.end())
    {
        id = wordLookup.size(); //assign consecutive numbers using size()
        wordLookup[word] = id;
    }
    else
    {
        id = it->second;
    }
    return id;
}

void tokenizeWords(std::map<std::string, unsigned int>& wordLookup, std::vector<unsigned int>& wordList, std::string& line)
{
    static const char newsDelimiters[] =  "., !?\"()'\n\r\t<>/\\";
    char str[line.size()];
    strncpy(str, line.c_str(), line.size());

    // Getting the first token
    char *token = strtok(str, newsDelimiters);
    while (token != NULL)
    {
        //finding a word:
        unsigned int id = addWord(wordLookup, token);
        wordList.push_back(id);

        // Getting the next token
        // If there are no tokens left, NULL is returned
        token = strtok(NULL, newsDelimiters);
    }
}


int main()
{
    std::vector<std::vector<unsigned int>> textAsNumbers;
    std::map<std::string, unsigned int> wordLookup;
    std::vector<std::string> searchWords = {"this", "blog", "political", "debate", "climate", "iphone"};
    unsigned int searchLength = searchWords.size();
    unsigned int searchWordIds[searchLength];

    //convert searchWords
    unsigned int i = 0;
    for(const std::string& word : searchWords)
    {
        searchWordIds[i] = addWord(wordLookup, word);
        ++i;
    }


    //#### This part is not time critical ####
    //reading file and convert words to numbers
    fstream newsFile;
    newsFile.open("news.txt",ios::in);
    if (newsFile.is_open())
    {
        string line;
        while(getline(newsFile, line))
        {
            textAsNumbers.push_back(std::vector<unsigned int>());
            std::vector<unsigned int>& wordList = *textAsNumbers.rbegin();
            tokenizeWords(wordLookup, wordList, line);
        }
        newsFile.close();
    }

    //#### This part should be fast ####
    auto start = std::chrono::system_clock::now();
    std::vector<unsigned int> counts; //end result
    counts.reserve(textAsNumbers.size());
    for(std::vector<unsigned int>& line : textAsNumbers)
    {
        unsigned int count = 0;
        for(unsigned int word : line)
        {
            for(unsigned int s = 0; s < searchLength; ++s)
            {
                unsigned int searchWord = searchWordIds[s];
                if(word == searchWord)
                {
                    ++count;
                }
            }
        }
        counts.push_back(count);
    }
    auto end = std::chrono::system_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    cout << elapsed.count() << "ms" << endl;

    //#### Print for checking result, time insensitive :)
    int n = 0;
    cout << "Counts:";
    for(unsigned int count : counts)
    {
        cout << count;
        ++n;
        if(n > 20)
        {
            break;
        }
        cout << ", ";
    }
    cout << endl;
}
